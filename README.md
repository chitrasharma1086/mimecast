# README #


### What is this repository for? ###

* Test assignment for Mimecast
* Version : 1.0.0
* Development and tested on Node v4.3.1

### How do I get set up? ###

* To set up Node modules run 'npm install' and 'npm run setup'

### How to run tests
* To run tests 'npm run test'