//login-assertions.js
module.exports = {

    assertLoginMandatoryElement: function (loginPage) {
        expect(loginPage.txt_emailAddress_isDisplayed()).toBe(true);
        expect(loginPage.txt_emailAddress_isEnabled()).toBe(true);
        console.log('ASSERT : Email Address is displayed and enabled on login page');
        expect(loginPage.btn_Next_isDisplayed()).toBe(true);
        expect(loginPage.btn_Next_isEnabled()).toBe(false);
        console.log('ASSERT : Next button displayed but disabled');
    },

    assertLoginAdditionalElement: function (loginPage, loginAsDifUserAppText, forgotPasswordAppText) {
        expect(loginPage.txt_emailAddress_isDisplayed()).toBe(true);
        expect(loginPage.txt_emailAddress_isEnabled()).toBe(false);
        console.log('ASSERT : Email Address is displayed and disabled');
        expect(loginPage.txt_password.isDisplayed()).toBe(true);
        expect(loginPage.txt_password.isEnabled()).toBe(true);
        console.log('ASSERT : Email Password is displayed and enabled');
        expect(loginPage.btn_login_isDisplayed()).toBe(true);
        expect(loginPage.btn_logIn_isEnabled()).toBe(false);
        console.log('ASSERT : Login button is displayed but disabled');
        expect(loginPage.appText_lnk_logInAsDifUser()).toEqual(loginAsDifUserAppText);
        console.log('ASSERT : Apptext for link Login as different user is verified ');
        expect(loginPage.appText_lnk_forgotPassword()).toEqual(forgotPasswordAppText);
        console.log('ASSERT : Apptext for Forgot Password link verified');
    },

    assertForgotPassordPageElement: function (loginPage, forgotPasswordPage, takeMeBackAppText, resetPasswordAppText) {
        expect(loginPage.txt_emailAddress_isDisplayed()).toBe(true);
        expect(loginPage.txt_emailAddress_isEnabled()).toBe(true);
        console.log('ASSERT : Email Address is displayed and enabled');
        expect(forgotPasswordPage.btn_resetPassword_isDisplayed()).toBe(true);
        console.log('ASSERT : Reset Button is displayed');
        expect(forgotPasswordPage.apptext_btn_resetPassword()).toBe(resetPasswordAppText);
        console.log('ASSERT : Reset Button app text verified');
        expect(forgotPasswordPage.lnk_takeMeBack_isDisplayed()).toBe(true);
        console.log('ASSERT : Take me back link displayed');
        expect(forgotPasswordPage.apptext_lnk_takeMeBack()).toBe(takeMeBackAppText);
        console.log('ASSERT : Forgot password link navigation verified');

    },

    assertLoginPageNavigation: function (loginPage, appText) {
        expect(loginPage.txt_password.isDisplayed()).toBe(true);
        expect(loginPage.txt_password.isEnabled()).toBe(true);
        console.log('ASSERT : Password field is displayed and enabled');
        expect(loginPage.appText_lnk_logInAsDifUser()).toEqual(appText);
        console.log('ASSERT : Apptext for link Login as different user verified');
    },

    assertInvalidLoginMsg: function (loginPage, msg) {
        expect(loginPage.err_msz_isDisplayed()).toBe(true);
        console.log('ASSERT : Error message for invalid login displayed');
        expect(loginPage.appText_err_msz()).toBe(msg);
        console.log('ASSERT : Apptext for error message verified');
    },

    assertLoginAsDiffUser: function (loginPage, appText) {
        expect(loginPage.txt_password.isEnabled()).toBe(true);
        console.log('ASSERT : Password field is enabled');
    },

};
