//home-assertions.js
module.exports = {
    assertSuccessfulLogin: function (homePage) {
        expect(homePage.btn_refresh_isDisplayed()).toBe(true);
        console.log('ASSERT : Refresh Button displayed on Inbox Page');
        expect(homePage.btn_compose_isDisplayed()).toBe(true);
        console.log('ASSERT : Compose Button displayed on Inbox Page');
    },
};