//email-assertions.js
module.exports = {
    assertSentItemsEmail: function (sentItemsPage, recepientEmail, subject, randomString) {
        console.log('INFO : User clicked latest sent email');
        expect(sentItemsPage.get_latestEmail_Receiver()).toEqual(recepientEmail);
        console.log('ASSERT : Email received verified');
        expect(sentItemsPage.get_latestEmail_Subject()).toEqual(subject);
        console.log('ASSERT : Email subject verified');
        expect(sentItemsPage.get_latestEmail_Content()).toEqual(randomString);
        console.log('ASSERT : Email content verified');
    },
};