/* test.specs.js */
var LoginPage = require('../page-objects/login.pageObject.js');
var BasePage = require('../page-objects/base.pageObject.js');
var ForgotPasswordPage = require('../page-objects/forgotPassword.pageObject.js');
var HomePage = require('../page-objects/home.pageObject.js');
var EmailComposerPage = require('../page-objects/emailComposer.pageObject.js');
var SentItemsPage = require('../page-objects/sentItems.pageObject.js');
var testData = require('../test-data/testData.js');
var constant = require('../utility/constant.js');
var loginModule = require('../modules/login-module.js');
var logoutModule = require('../modules/logout-module.js');
var forgotPasswordModule = require('../modules/forgot-password-module.js');
var emailModule = require('../modules/email-module.js');
var loginAssertions = require('./login-assertions.js');
var homeAssertions = require('./home-assertions.js');
var emailAssertions = require('./email-assertions.js');

var loginPage = new LoginPage();
var basePage = new BasePage();
var forgotPasswordPage = new ForgotPasswordPage();
var homePage = new HomePage();
var emailComposerPage = new EmailComposerPage();
var sentItemsPage = new SentItemsPage();
var randomString = basePage.getRandomStringToEnterInEmailContent(20);

beforeEach(function () {
  basePage.getBaseURL();
  console.log(' -----Application loaded------');
});

describe('Login page elements presence and navigation verification', function () {
  it('should verify mandatory elements present on login page', function () {
    loginAssertions.assertLoginMandatoryElement(loginPage);
  });

  it('should verify additional elements present on login page', function () {
    loginModule.clickNext(loginPage, testData.inValidPasswordUser.username).
      then(function () {
        console.log('INFO : User clicked Next button');
        loginAssertions.assertLoginAdditionalElement(loginPage, testData.logInAsDiffUserLink.appText, testData.forgotPasswordLink.appText);
      })
  });

  it('should verify elements presence and successful link navigations on forgot password page', function () {
    forgotPasswordModule.clickForgotPassword(loginPage, testData.inValidPasswordUser.username).
      then(function () {
        console.log('INFO : User clicked Forgot Password Page');
        loginAssertions.assertForgotPassordPageElement(loginPage, forgotPasswordPage, testData.takeMeBackLink.appText, testData.resetPassword.appText);
      })
  });
}),

  describe('Login Page links navigation verification', function () {
    it('should verify successful Next button link navigation on login Page ', function () {
      loginModule.clickNext(loginPage, testData.inValidPasswordUser.username).
        then(function () {
          console.log('INFO : User clicked Next button');
          loginAssertions.assertLoginPageNavigation(loginPage, testData.logInAsDiffUserLink.appText);
        })
    });

    it('should verify successful Login as different user link navigation on login Page', function () {
      loginModule.clickLoginAsDiffUser(loginPage, testData.inValidPasswordUser.username).
        then(function () {
          console.log('INFO : User clicked login as different user link');
          loginAssertions.assertLoginMandatoryElement(loginPage);
        })
    });

    it('should verify successful take me back link navigation on forgot password page', function () {
      forgotPasswordModule.clickTakeMeBack(loginPage, testData.inValidPasswordUser.username, forgotPasswordPage).
        then(function () {
          console.log('INFO : User clicked take me back link');
          loginAssertions.assertLoginAdditionalElement(loginPage, testData.logInAsDiffUserLink.appText, testData.forgotPasswordLink.appText);
        })
    });
  });

describe('Unsuccessful login when either username or password is not valid', function () {
  it('should verify user cannot login with invalid user name', function () {
    loginModule.login(loginPage, testData.inValidEmailUser.username, testData.inValidEmailUser.password).
      then(function () {
        console.log('INFO : Login failed');
        loginAssertions.assertInvalidLoginMsg(loginPage, testData.errorMessage.appText);
      })
  });

  it('should verify user cannot login with invalid password', function () {
    loginModule.login(loginPage, testData.inValidPasswordUser.username, testData.inValidPasswordUser.password).
      then(function () {
        console.log('INFO : Login failed');
        loginAssertions.assertInvalidLoginMsg(loginPage, testData.errorMessage.appText);
      })
  });
});

describe('Successful login when login details are correct', function () {
  it('should verify successful login on valid username and password', function () {
    loginModule.login(loginPage, testData.validTestUser.username, testData.validTestUser.password).
      then(function () {
        console.log('INFO : Login successful');
        homeAssertions.assertSuccessfulLogin(homePage);
        logoutModule.logout(homePage);
      })
  });
});

describe('Sent Email functionality verification', function () {
  it('should verify email succesfully sent from application', function () {
    emailModule.loginComposeAndSendMail(loginPage, testData.validTestUser.username, testData.validTestUser.password, homePage, emailComposerPage, testData.emailSuccessDetails.recepientEmail, testData.emailSuccessDetails.subject, randomString).
      then(function () {
        console.log('INFO : User clicked send mail button');
        emailModule.fetchLatestMailInSentItems(homePage,sentItemsPage).then(function () {
        console.log('ASSERT : Inbox mail received the recently sent mail');
        emailAssertions.assertSentItemsEmail(sentItemsPage, testData.emailSuccessDetails.recepientEmail, testData.emailSuccessDetails.subject, randomString);
        logoutModule.logout(homePage);
        })
      })
  });
});





