//home.pageObject.js
var HomePage = function () {

    this.btn_refresh = element(by.xpath('//*[@id="ng-app"]/body/div[5]/div[3]/div[2]/div/ul/li[1]/a'));
    this.btn_compose = element(by.xpath('//*[@id="main-action"]/button'));
    this.lnk_inbox = element(by.xpath('//*[@id="inbox"]/a'));
    this.lnk_sent_Email = element.all(by.xpath('//*[@id="sent"]/a')).get(1);
    this.lnk_userProfile = element(by.xpath('//*[@id="navbar"]/div/div/div[2]/ul/li[3]/a'));
    this.btn_logout = element(by.buttonText('Log Out'));

    this.btn_refresh_isDisplayed = function () {
        return this.btn_refresh.isDisplayed();
    };

    this.btn_compose_isDisplayed = function () {
        return this.btn_compose.isDisplayed();
    };

    this.click_btn_compose = function () {
        return this.btn_compose.click();
    };

    this.click_lnk_sent_Email = function () {
        return this.lnk_sent_Email.click();
    };

    this.click_lnk_userProfile = function () {
        return this.lnk_userProfile.click();
    };

    this.click_btn_logout = function () {
        return this.btn_logout.click();
    };

    return this;
};

module.exports = HomePage;