/* login.pageObjects.js */
var LoginPage = function () {

    this.txt_emailAddress = element(by.id('username'));
    this.txt_password = element(by.id('password'));
    this.btn_next = element(by.xpath('//*[@id="ng-app"]/body/div[3]/div/div[2]/div/div/div[1]/div[1]/div/div/form/button[1]'));
    this.btn_logIn = element(by.xpath('//*[@id="ng-app"]/body/div[3]/div/div[2]/div/div/div[1]/div[1]/div/div/form/button[1]'));
    this.lnk_logInAsDifUser = element(by.buttonText('Log in as a different user.'));
    this.lnk_forgotPassword = element(by.buttonText('Forgot your password?'));
    this.err_msz = element(by.binding('appCtrl.errorMessage'));

    this.set_EmailId = function (username) {
        return this.txt_emailAddress.sendKeys(username);
    };

    this.set_Password = function (password) {
        return this.txt_password.sendKeys(password);
    };

    this.click_NextBtn = function () {
        return this.btn_next.click();
    };

    this.click_logInAsDifUser = function () {
        return this.lnk_logInAsDifUser.click();
    };
    this.appText_lnk_logInAsDifUser = function () {
        return this.lnk_logInAsDifUser.getText();
    };

    this.txt_emailAddress_isEnabled = function () {
        return this.txt_emailAddress.isEnabled();
    };

    this.txt_emailAddress_isDisplayed = function () {
        return this.txt_emailAddress.isDisplayed();
    };

    this.btn_logIn_isEnabled = function () {
        return this.btn_logIn.isEnabled();
    };

    this.btn_login_isDisplayed = function () {
        return this.btn_logIn.isDisplayed();
    };

    this.click_btn_login = function () {
        return this.btn_logIn.click();
    };

    this.btn_Next_isDisplayed = function () {
        return this.btn_next.isDisplayed();
    };

    this.btn_Next_isEnabled = function () {
        return this.btn_next.isEnabled();
    };

    this.click_lnk_forgotPassword = function () {
        return this.lnk_forgotPassword.click();
    };

    this.appText_lnk_forgotPassword = function () {
        return this.lnk_forgotPassword.getText();
    };

    this.err_msz_isDisplayed = function () {
        return this.err_msz.isDisplayed();
    };

    this.appText_err_msz = function () {
        return this.err_msz.getText();
    };

    return this;
};
module.exports = LoginPage;

