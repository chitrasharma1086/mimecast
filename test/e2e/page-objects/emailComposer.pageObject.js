//emailComposer.pageObject.js
var EmailPage = function () {

    this.txt_recepient = element(by.xpath('//*[@id="sendEmailForm"]/div[1]/div[2]/div[1]/div[2]/div/div[1]/input'));
    this.lnk_add_recepient = element(by.xpath('//*[@id="sendEmailForm"]/div[1]/div[2]/div[1]/div[2]/div/div[2]/div'));
    this.txt_subject = element(by.xpath('//*[@id="sendEmailForm"]/div[1]/div[2]/div[3]/div[2]/input'));
    this.txt_msz = element(by.xpath('//*[@id="sendEmailForm"]/div[3]/div[2]/div[6]'));
    this.btn_send = element(by.xpath('//*[@id="sendEmailForm"]/div[1]/div[1]/div/div[1]/button'));

    this.set_recipientEmailAddress = function (emailAddress) {
        return this.txt_recepient.sendKeys(emailAddress);
    };

    this.email_to_add_isDisplayed = function () {
        return this.lnk_add_recepient.isDisplayed();
    };

    this.email_to_add_click = function () {
        return this.lnk_add_recepient.click();
    };

    this.set_subject = function (subjectLine) {
        return this.txt_subject.sendKeys(subjectLine);
    };

    this.set_message = function (mszToSend) {
        return this.txt_msz.sendKeys(mszToSend);
    };

    this.click_btn_send = function () {
        return this.btn_send.click();
    };

    return this;

};

module.exports = EmailPage;