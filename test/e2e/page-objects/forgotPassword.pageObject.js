//forgotPassword.pageObject.js
var ForgotPasswordPage = function () {

    this.txt_emailAddress = element(by.id('username'));
    this.btn_resetPassword = element(by.xpath('//*[@id="ng-app"]/body/div[3]/div/div[2]/div/div/div[1]/div[1]/div/div/form/button[1]'));
    this.lnk_takeMeBack = element(by.xpath('//*[@id="ng-app"]/body/div[3]/div/div[2]/div/div/div[1]/div[1]/div/div/form/div[2]/button'));

    this.txt_emailAddress_isDisplayed = function () {
        return this.txt_emailAddress.isDisplayed();
    }

    this.btn_resetPassword_isDisplayed = function () {
        return this.btn_resetPassword.isDisplayed();
    }

    this.apptext_btn_resetPassword = function () {
        return this.btn_resetPassword.getText();
    }

    this.lnk_takeMeBack_isDisplayed = function () {
        return this.lnk_takeMeBack.isDisplayed();
    }

    this.click_lnk_takeMeBack = function () {
        return this.lnk_takeMeBack.click();
    }

    this.apptext_lnk_takeMeBack = function () {
        return this.lnk_takeMeBack.getText();
    }

};
module.exports = ForgotPasswordPage;