//base.pageObject.js
var BasePage = function () {

    var constant = require('../utility/constant.js');
    var EC = protractor.ExpectedConditions;

    this.getBaseURL = function () {
        return browser.get(constant.baseURL.URL);
    };

    this.waitForElement = function (element) {
        return browser.wait(EC.visibilityOf(element), 7000);
    };

    this.getRandomStringToEnterInEmailContent = function (length) {
        var string = '';
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' //Include numbers if you want
        for (i = 0; i < length; i++) {
            string += letters.charAt(Math.floor(Math.random() * letters.length));
        }
        return string;
    };
};
module.exports = BasePage;


