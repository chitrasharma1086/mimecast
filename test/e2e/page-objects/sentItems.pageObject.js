//sentItems.pageObject.js
var SentItemsPage = function () {

    this.lnk_latestEmail = element(by.xpath('//*[@id="ng-app"]/body/div[5]/div[3]/div[2]/div/div/div/mc-list-detail/div/div[1]/div[2]/div/table/tbody/tr[1]/td[2]'));
    this.latestEmail_Receiver = element(by.xpath('//*[@id="ng-app"]/body/div[5]/div[3]/div[2]/div/div/div/mc-list-detail/div/div[3]/div/div[2]/div/detail/div[2]/div/div/div/div/ul/li[3]/ul/li/span[2]/span'));
    this.latestEmail_Subject = element(by.xpath('//*[@id="ng-app"]/body/div[5]/div[3]/div[2]/div/div/div/mc-list-detail/div/div[3]/div/div[2]/div/detail/div[1]/div/div/h4'));
    this.latestEmail_Content = element(by.xpath('//*[@id="ng-app"]/body/div[5]/div[3]/div[2]/div/div/div/mc-list-detail/div/div[3]/div/div[2]/div/detail/div[6]/div/div'));

    this.click_lnk_latestEmail = function () {
        return this.lnk_latestEmail.click();
    };

    this.get_latestEmail_Receiver = function () {
        return this.latestEmail_Receiver.getText();
    };

    this.get_latestEmail_Subject = function () {
        return this.latestEmail_Subject.getText();
    };

    this.get_latestEmail_Content = function () {
        return this.latestEmail_Content.getText();
    };

    return this;
};

module.exports = SentItemsPage;