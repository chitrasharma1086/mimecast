//testData.js
var testData = {

    validTestUser: { 'username': 'chitrasharma1086@gmail.com', 'password': 'Testing@123' },
    inValidPasswordUser: { 'username': 'chitrasharma1086@gmail.com', 'password': 'InvalidPassword' },
    inValidEmailUser: { 'username': 'invalidDummyUser@gmail.com', 'password': 'Testing@123' },
    errorMessage: { 'appText': 'Invalid user name, password or permissions.' },
    logInAsDiffUserLink: { 'appText': 'Log in as a different user.' },
    forgotPasswordLink: { 'appText': 'Forgot your password?' },
    takeMeBackLink: { 'appText': 'Never mind, take me back to the login page.' },
    resetPassword: { 'appText': 'Reset Password' },
    emailSuccessDetails: { 'recepientEmail': 'testrecepient@mimecast.com', 'subject': 'To Test', 'msz': 'Testing send email functionailiy' },

};

module.exports = testData;