//forgot-password-module.js
var self = {

    //Function to click 'Forgot password' link
    clickForgotPassword: function (loginPage, username) {
        return loginPage.set_EmailId(username).then(function () {
            console.log('INFO : User entered valid format Email Address')
            return loginPage.click_NextBtn().then(function () {
                console.log('INFO : User clicked Next button');
                return loginPage.click_lnk_forgotPassword();
            })
        }).catch(function (error) {
            console.log('Unable to click forgot password link', error);
            return Promise.reject();
        })
    },

    //Function to click 'Never mind, take me back to the login page.' link
    clickTakeMeBack: function (loginPage, username, forgotPasswordPage) {
        return loginPage.set_EmailId(username).then(function () {
            console.log('INFO : User entered valid format Email Address');
            return loginPage.click_NextBtn().then(function () {
                console.log('INFO : User clicked Next button')
                return loginPage.click_lnk_forgotPassword().then(function () {
                    console.log('INFO : User clicked on forgot password link');
                    return forgotPasswordPage.click_lnk_takeMeBack();
                })
            })
        }).catch(function (error) {
            console.log('Unable to click take me back link', error);
            return Promise.reject();
        })
    },
};
module.exports = self;