//login-module.js
var self = {
    //Function to click next button 
    clickNext: function (loginPage, username) {
        return loginPage.set_EmailId(username).then(function () {
            console.log('INFO : User entered Email Address')
            return loginPage.click_NextBtn();
        }).catch(function (error) {
            console.log('Unable to click Next button', error);
            return Promise.reject();
        })
    },

    //Function to login to the application
    login: function (loginPage, username, password) {
        return self.clickNext(loginPage, username).then(function () {
            console.log('INFO : User clicked Next button')
            return loginPage.set_Password(password).then(function () {
                console.log('INFO : User entered password');
                return loginPage.click_btn_login();
            })
        }).catch(function (error) {
            console.log('Unable to login', error);
            return Promise.reject();
        })
    },

    //Function to click 'Login as different User' link
    clickLoginAsDiffUser: function (loginPage, username) {
        return self.clickNext(loginPage, username).then(function () {
            console.log('INFO : User clicked Next button')
            return loginPage.click_logInAsDifUser();

        }).catch(function (error) {
            console.log('Unable to click login as different user link', error);
            return Promise.reject();
        })
    },

};
module.exports = self;
