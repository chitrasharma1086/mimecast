//email-module.js
var loginModule = require('../modules/login-module.js');
var BasePage = require('../page-objects/base.pageObject.js');

var basePage = new BasePage();
var self = {

    //Function to login, compose and send email
    loginComposeAndSendMail: function (loginPage, username, password, homePage, emailComposerPage, recepient, subject, msz) {
        return loginModule.login(loginPage, username, password).
            then(function () {
                console.log('INFO : User logged In');
                return self.composeAndSendMail(homePage, emailComposerPage, recepient, subject, msz);
            }).catch(function (error) {
                console.log('loginComposeAndSendMail failed', error);
                return Promise.reject();
            });
    },

    //Function to compose and send email
    composeAndSendMail: function (homePage, emailComposerPage, recepient, subject, msz) {
        return basePage.waitForElement(homePage.btn_compose).then(function () {
            return homePage.click_btn_compose().then(function () {
                console.log('INFO : User clicked on compose button');
                return self.sendMail(emailComposerPage, recepient, subject, msz);
            })
        }).catch(function (error) {
            console.log('composeAndSendMail failed', error);
            return Promise.reject();
        });
    },
    //Function to send email
    sendMail: function (emailComposerPage, recepient, subject, msz) {
        return emailComposerPage.set_recipientEmailAddress(recepient).
            then(function () {
                console.log('INFO : User entered recepient email address');
                return emailComposerPage.lnk_add_recepient.isDisplayed().
                    then(function () {
                        return emailComposerPage.lnk_add_recepient.click().
                            then(function () {
                                return emailComposerPage.set_subject(subject).
                                    then(function () {
                                        console.log('INFO : User entered subject');
                                        return emailComposerPage.set_message(msz).
                                            then(function () {
                                                console.log('INFO : User entered message');
                                                return emailComposerPage.click_btn_send();
                                            })
                                    })
                            })
                    })
            }).catch(function (error) {
                console.log('composeAndSendMail failed', error);
                return Promise.reject();
            });
    },

    fetchLatestMailInSentItems: function (homePage,sentItemsPage) {
        return homePage.click_lnk_sent_Email().then(function () {
            console.log('INFO : User clicked Sent Items box');
            return sentItemsPage.click_lnk_latestEmail().
                then(function (param) {
                    
                })
        }, 2000).catch(function (error) {
            console.log('loginComposeAndSendMail failed', error);
            return Promise.reject();
        });
    },


};
module.exports = self;
