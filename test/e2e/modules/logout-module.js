//logout-module.js
var self = {
    
    // Function to logout
    logout: function (homePage) {
        return homePage.click_lnk_userProfile().then(function () {
            console.log('INFO: User profile clicked');
            homePage.click_btn_logout().then(function () {
                console.log('INFO: User logged out');
            })
        })
    }
};
module.exports = self;
