exports.config = {

    //seleniumAddress: 'http://localhost:4444/wd/hub',

    specs: ['./test/e2e/test-specs/test.spec.js'],
    capabilities: {
        browserName: 'chrome',
    },

    getPageTimeout: 30000,
    allScriptsTimeout: 30000,
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 1000000
    },

    onPrepare: function () {
        var jasmineReporters = require('jasmine-terminal-reporter');
        jasmine.getEnv().addReporter(new jasmineReporters({
            isVerbose: true
        }));
    }

};